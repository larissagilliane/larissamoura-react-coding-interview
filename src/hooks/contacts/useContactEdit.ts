import { IPerson } from '@lib/models/person';
import { useCallback } from 'react';
import {contactsClient} from "@lib/clients/contacts";

export function useContactEdit(id: string) {
  console.log(`Getting contact by id ${id}`);

  const fetchContactById = useCallback(async () => {
    const res = await contactsClient.getContactById(id);
    console.log(res, 'contactsClient');
  }, []);

  return {
    contact: null as IPerson,
    update: fetchContactById
  };
}
